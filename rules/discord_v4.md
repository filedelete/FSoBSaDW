# FAN DISCORD SERVER OF BLACK SILVER AND DARIYA WILLIS

## [Discord сервер](../info/about_discord.md) - [Правила версии 3.1](discord_v3.md) (**обновляемые**)

|   |   |
|---|---|
| [![Red warning icon](../info/img/red_warn.png)](#red_warn) | Данная версия правил находятся в стадии разработки. На текущий момент по-прежнему действует версия правил 3.1 |
| [![Yellow warning icon](./rules_img/radioactive.png)](#red_warn) | Все, что здесь написано может ещё кучу раз поменяться. |

| | **Мы обновили правила!** |
|---------------|---------------|
| [![Green happy icon](./rules_img/good.png)](#A1) | Произошло достаточно много изменений в правилах нашего любимого сервера! Пожалуйста, изучите все правила заново. |

## Общие правила поведения

| [![Orange message failed icon](./rules_img/message_error.png)](#L1RS1) | **Оскорбления участников, спам, флуд.** |
|---------|---------|
| **Оскорбления** | В чатах не стоит никого оскорблять, даже в шутку не рекомендуется. За оскорбления мут от 8 часов до перманентного бана по усмотрению модераторов |
| **Спам** | За спам сразу после входа - перманентный бан. Обычному участнику: мут → бан на неделю → перманентный бан. Смотрите также: [определение спама](definitions.md) |
| **Флуд** | За флуд после входа - перманентный бан. Обычному участнику: мут на 8 часов → бан на неделю → перманентный бан. Смотрите также: [определение флуда](definitions.md) |
| **Лицемерие** | мы крайне не одобряем. Если вам что-то не нравится на сервере - сообщите нам и мы попробуем решить проблему вместе. Если мы ничего не меняем, то покиньте его, в противном случае мы произведем "добровольное отключение" (перманентный бан) |

| [![Red message failed icon](./rules_img/messages_content.png)](#L1RS2) | **Нецензурная брань, шок-контент** |
|---------|---------|
| **Нецензурная брань** | На сервере крайне не рекомендуется использовать мат в своих сообщениях и речи. А также употреблять его в отношение других людей. Даже если вы в шутку написали `@someone и что ###ть?` - ждите мут, а затем бан |
| **Шок-контент** | А тут намного проще: шок-контент = бан на неделю, второе нарушение - перманентный бан. Все изображения проверяются нейронной сетью и содержащие шок-контент будут мгновенно отправлены в Discord, а также удалены из чатов. Распространение порнографии запрещено [правилами Discord](https://discordapp.com/tos) и приведет к бану аккаунта и IP адреса. |

| [![Neutral face](./rules_img/sentiment_neutral.png)](#L1RS3) | **Споры, критика без аргументов, [троллинг](https://ru.wikipedia.org/wiki/%D0%A2%D1%80%D0%BE%D0%BB%D0%BB%D0%B8%D0%BD%D0%B3)** |
|---------|---------|
| **Споры** | Мы разрешаем спорить в чатах если это не переходит в конфликт. Конфликты с агрессией = мут-предупреждение → мут → бан (неделя) → бан (перманентный) |
| **Критика без аргументов** | Критика человека без основательных аргументов = мут-предупреждение → мут (8 часов) → бан (перманентный) |
| **Троллинг (провокации, издевательства)** | Троллинг участников сервера, провокации и издевательства над ними приведут к перманентному бану. |

| [![Flag icon](./rules_img/flag.png)](#L1RS4) | **Политика, нация, ориентация** |
|---------|---------|
| **Споры по политике, о национальности, ориентации и др.** | запрещены на сервере. За обсуждение этих тем следует мут, а затем бан. |

| [![Mail icon](./rules_img/mail.png)](#L1RS5) | **Личные сообщения** |
|---------|---------|
| **ЛС** | Правила сервера не регулируют ЛС. Вы можете разводить там срач, угрожать, кидать шок-контент и т.п. Если вам угрожают в ЛС кто-то из участников сервера, то просто заблокируйте его, а также отправьте сообщение Discord: для этого используйте ПКМ на компьютере или обратитесь к службе поддержке Discord по адресу `support@discordapp.com`. То же самое относится ко [спаму](definitions.md). Не игнорируйте, действуйте против! |
| **Реклама** | В случае рассылки рекламы участникам сервера мы выдаем бан рассыльщику и отправляем его в список глобал банов всех наших партнерских серверов. Смотрите также: [определение рекламы](definitions.md) |
| **Администрация** | Вы можете спокойно связаться с администрацией для решения того или иного вопроса. Но! Сообщение (и упомянание) без особого повода (мут, бан) администратора имеющего статус "отошел" (желтая точка) или "занят" (красная точка) приведет к муту на 8 часов. |
| **Модераторы** | По поводу нарушений правил сервера пишите активным модераторам (они имеют зеленую точку) в ЛС. Если нарушение требует срочного реагирования, упомянайте их роль `@Модераторы`. За разъяснением причин мута или бана - обращайтесь к активным администраторам. Упоминание роли или модератора без особого повода (исключая ответы на сообщения в течение часа) приведет к муту на 8 часов. |

## Правила текстовых каналов

<!--
    СЕКЦИЯ 2 | ПК
    Правила всех текстовых каналов
-->

### Основные правила текстовых каналов

| [![Smiley icon](./rules_img/reaction.png)](#L1RS6) | **Злоупотребление реакциями** |
|---------|---------|
| **Злоупотребление реакциями** | Не злоупотребляйте реакциями к сообщениям. Это приведет к выдаче NoReactions. Смотрите также: [определение "NoReactions"](definitions.md) |

| [![Quote icon](./rules_img/format_quote.png)](#L2S1RS1) | **Цитирование / упоминания** |
|---------|---------|
| **Цитирование** | Все цитаты выделяются кавычками и по возможности дописывается, кто это сказал |
| **Маты** | Маты в цитатах закрываются символом решетки `#` хотя бы на 70% |
| **Упоминание модераторов и администраторов** | Упоминание модераторов в чате допускается при нарушениях, в остальных случаев будет следовать наказание. **Упоминание администрации и модераторов без весомой причины приведет к бану, внимательно изучите пункт "Личные Сообщения"!** |
| **Спойлеры** | Распростронение спойлеров запрещено |

| [![Format size icon](./rules_img/format_size.png)](#L2S1RS2) | **Неграмотное форматирование текста** |
|---------|---------|
| **Неграмотное форматирование текста** | Это попытка выделения своего текста путем выделения его жирным, курсивным, подчеркивая. Не путайте обычное форматирование, где оно используется для выделения ключевых фраз, действий, чтобы познать неграмотное форматирование текста, отправьте сообщение с текстом из примера ниже любому боту |

Пример очень агрессивного неграмотного форматирования текста:

````md
:lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon:
:pineapple: **__*ХЭЙ РЕБЯТА, Я ПРОСТО ТОПЧИК ЗНАЙТЕ ВСЕ ЭТО*__** :pineapple:
:pineapple: **__*А ТЫ НЕ ТОПЧИК, А Я ТОПЧИК. ХАХАХХАХАХ*__** :pineapple:
:pineapple:  ~~*__**`ЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕ`**__*~~ :pineapple:
:lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon: :lemon:
````

| [![Format size icon](./rules_img/gear.png)](#L2S1RS3) | **Использование ботов** |
|---------|---------|
| **Использование команд ботов** | Использование команд ботов производится в специальных каналах для этих ботов или в хаосе. В основных чатах команды должны использоватся крайне редко (но могут, если они по теме и не отключены администрацией) |
| **Rocky!** | Ставить абсолютную бессмыслицу и бредовые видео в Rocky! запрещено. В случае нарушения - запрет на отправку сообщений в `#rocky_station` |
| **Tatsumaki** | Управление профилем, ранки и всё-всё связанное с Tatsumaki производится в текстовом канале `#bots-commands` |
| **!ping** | Использование данной команды в обычных чатах приравнивается к флуду |

### Отдельные правила каналов

<!--
УДАЛЕНО: #newsfeed теперь закрытый канал
| [![New release icon](./rules_img/new.png)](#L2S2RS1) | `#newsfeed` |
|---------|---------|
| **Баяны** | Прежде чем опубликовать - посмотрите, не было ли точно такого же поста несколькими сообщениями выше.. При обнаружении "баянов" - мут-предупреждение, второй раз = запрет на отправку сообщений в канал `#feed` |
| **Единая Россия** | И вообще политика - это не здесь, пожалуйста. Сразу удаление сообщения, бан на 1 день |
| **`@everyone`** | Вам недоступно, даже не пытайтесь |-->

| [![Send message icon](./rules_img/send.png)](#L2S2RS2) | **Любые `-share` каналы** |
|---------|---------|
| **Чат** | Ни более одного сообщения с комментарием на всех посмотревших. Остальное запрещено. За несоблюдения выдаётся мут-предупреждение → мут (8 часов) → бан на неделю |
| **Отдельные правила** | Каждый `-share` канал устанавливает для себя отдельные правила. Например, в `#music-share` разрешено отправлять ссылки только на YouTube, Soundcloud, Spotify, Deezer. Смотрите их в [закрепленных сообщениях](https://i.imgur.com/xTmJJ8q.png). |
| **Селфи** | Мы одобряем ваши селфи, они немногим делают сервер немного приятнее, поддерживают атмосферу дружбы. Но, селфи необходимо публиковать в специальный канал - `#selfie`. За селфи в чате (не по теме) следует мьют, а за селфи в `#pic-share` может быть ограничен доступ к отправке сообщений. |
| **Загрязнение канала** | Скриншоты, фотографии и другое предназначенное для людей в чате отправляется в этот же чат, если это для людей в голосовых каналах - оно отправляется в `#voice-chat`! Загрязнение `-share` каналов повлечет к отключению этих каналов (отключение отправки сообщений) |

| [![Person pin icon](./rules_img/person_pin.png)](#L2S2RS3) | **`#name`** |
|---------|---------|
| **Чат** | Общение в любой форме запрещено. Это просто канал для ваших анкет. Несоблюдение приведет к муту-предупреждению → муту (8 часов) → бану |
| **Самореклама** | Мы разрешаем вам показать ваши работы, ваш YouTube или Twitch канал, но если вы не будете прямо намекать на подписку или просмотр. Не запрещено так же отправлять ссылки на свои работы, быть может кто-то найдёт себе фаворита-художника или прекрасного дизайнера на работу! В остальных случаях анкета будет удалена (мы отправим Вам её текст в ЛС для редактирования), Вы получите мут-предупреждение → мут (8 часов) → бан |

| [![Flash pin icon](./rules_img/flash.png)](#L2S2RS4) | **`#bots-commands` - Команды ботов** |
|---------|---------|
| **Общение** | Общение в `#bots-commands` запрещено. В том числе и с помощью команд (например, при выдаче печенек Tatsumaki). Для общения существует общий чат, голосовые каналы и чаты гильдий. За нарушение Вы получите мут-предупреждение → мут (8 часов) → бан (на неделю). |
| **1-2 ответа** | Если участник выдал вам репутацию Tatsumaki или сделал что-то полезное, то Вы можете ответить ему, но не более одного-двух сообщений (читаем выше). |

<!--| **Brainpower, !shib** | Изначально хаос назывался `#flood-area`, название говорит само за себя: канал для флуда |-->

## Правила голосовых каналов

<!--
    СЕКЦИЯ 3 | ПГК
    Правила всех голосовых каналов
-->

### Основные правила голосовых каналов

| [![Microphone muted icon](./rules_img/no_micro.png)](#L3S1RS1) | **Причины, почему микрофон отключают со стороны сервера** |
|---------|---------|
| **Сильный шум** | Если ваш микрофон будет сильно шуметь мы его отключим на стороне сервера |
| **AFK канал** | Сервер автоматически отключает ваш микрофон при входе в канал AFK |

| [![Microphone muted icon](./rules_img/lock.png)](#L3S1RS2) | **Причины, почему можно получить `NoVoice`** |
|---------|---------|
| **"Флуд" входом-выходом** | "Флуд" входом-выходом в голосовый канал присекается выдачей роли `NoVoice` |
| **Прыжки по каналам** | Прыгание по каналам из одного в другой также присекается выдачей роли `NoVoice` |

| [![Comment icon](./rules_img/comment.png)](#L3S1RS3) | **Чат** |
|---------|---------|
| **Ответы людям в голосовых каналах** | Для ответа людям в голосовых каналах при отсутствии микрофона используется текстовый канал `#voice_chat`. Сообщения отправленные людям в обычном чате приведут к муту-предупреждению → муту (8 часов) → бану  |
| **Rocky!** | Управление Rocky! производится в `#rocky-station`. В других чатах его команды не работают, в обычных чатах они попадают под определение "флуда". |

### Отдельные правила голосовых каналов

| [![Speaking man icon](./rules_img/speaking.png)](#L3S2RS1) | **Каналы общения** |
|---------|---------|
| **Игры** | Если вы играете, то идите в канал игры. Исключения составляют случаи, когда Вы не обсуждаете всё происходящее в игре, а просто общаетесь с другими людьми. |

| [![Videogame icon](./rules_img/videogame.png)](#L3S2RS2) | **Игры** |
|---------|---------|
| **Группы** | В голосовых каналах "Игры" объединяются по одной игре, т.е это не канал для обсуждения игр, а для игры вместе. Для обсуждения игр есть обычные каналы общения! |

<!--
УДАЛЕНО: Это бесполезные правила
| [![Group add icon](./rules_img/group_add.png)](#L3S2RS3) | **Набор для игр с подписчиками** |
|---------|---------|
| **Нужная игра** | Набор для игр с подписчиками производится только по игре, выбранной стримером. Т.е. если стример играет в Overwatch, то вы должны вступать в канал ТОЛЬКО если имеете купленный и открытый на рабочем столе Overwatch |
| **Overwatch** | Если у вас есть Overwatch и вы хотите поиграть со стримером, то откройте свой Battle.Net тег в настройках Discord (Пользовательские настройки > Интеграция > Battle.Net > Отображать в профиле) и не забудьте добавить стримера в Battle.Net |
| **Steam** | Если стример набирает игроков в игру, требующую отправление инвайта через Steam, то включите также отображение своего профиля Steam в Discord (Пользовательские настройки > Интеграция > Steam > Отображать в профиле) |
-->

| [![Video icon](./rules_img/video.png)](#L3S2RS4) | **Каналы стримов** |
|---------|---------|
| **PTT** | В каналах просмотра стримов включен режим PTT, чтобы избежать шума |
| **Не в основных каналах** | Стримы не смотрят в основных каналах общения. В случае первого нарушения модератор предупредит Вас. В противном случае может быть выдана роль NoVoice. |

## Правила нахождения на сервере

<!--
    СЕКЦИЯ 4 | ПННС
    Правила нахождения на сервере
-->

| [![Man and tree icon](./rules_img/man_and_tree.png)](#L4RS1) | **Нахождение на сервере** |
|---------|---------|
| **Оффлайн 1 месяц** | За оффлайн в течение месяца Вы можете быть выкинуты с сервера. В случае отъезда сообщайте модераторам об этом. |
| **Соглашение со всеми правилами** | Находясь на сервере Вы принимаете все его правила до единого. Если некоторые из правил Вас не устраивают, обратитесь к администрации (внимание на пункт "Личные Сообщения"). До принятия правил, Вы не должны писать сообщений в чат и вступать в голосые каналы. Если администрация не может решить Ваш вопрос с правилами, Вы должны покинуть сервер. |
| **3 предупреждения** | У Вас есть возможность три раза получать муты, четвертого предупреждения нет. Вместо 4 предупреждения - бан. Учтите, что за некоторые нарушение бан следует без предупреждения. |
| **Никнейм** | Ваш никнейм (или юзернейм) не должен содержать нецензурных слов, политических взглядов, имена чужих людей, символов юникода, оскорблений и другое на усмотрение администрации. Никнейм может содержать буквы А-Я, A-Z; цифры 0-9. Если после предупреждения модератора о смене ника, он не изменится вы будете кикнуты с сервера, а в след. раз забанены. |
| **Аватарки** | Аватарки так же не должны содержать нацисткой символики, эротики (наготы), оскорбительных лозунгов и другого (усмотрение модераторов). В случае нарушения Вы получите мут, модератор попросит Вас сменить его в личных сообщениях. |
| **Фейковые аккаунты** | Все дополнительные аккаунты будут забанены или кикнуты с сервера. Исключение составляют аккаунты, доступ к которым был потерян, тогда старый будет кикнут. |

Правила в стадии "Разработка".

**Нашли ошибку?** Сообщите администрации в Discord: `FSofBSaDW Admin#8512`.

Последнее обновление: .