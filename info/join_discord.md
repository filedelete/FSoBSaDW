# FAN DISCORD SERVER OF BLACK SILVER AND DARIYA WILLIS

## [![Иконка Discord](img/discord_icon_24x24.png) Discord сервер](about_discord.md) - [Вступление](join_discord.md)

***
Привет, рад, что тебе стало интересно, и ты захотел вступить. На нашем сервере ты сможешь общаться голосом, просто чатиться, узнавать о начавшихся стримах, публиковать и оценивать арты, играть вместе в разные игры, слушать музыку и многое другое!

[![Клубничка](img/strawberry.png)](#lalala) **16+**! Пожалуйста, обрати внимание, что тебе должно быть более или хотя бы 16 лет, иначе - покинь эту страницу, мы сожалеем, возвращайся как повзрослеешь.

Ты можешь вступить на сервер Discord без какой-либо регистрации (протестировать), просто нажми на картинку ниже и введи никнейм. Если тебе действительно станет интересно - зарегистрируйся, это позволит использовать Discord в полной мере.

[![Discord](https://discordapp.com/api/guilds/209767264210255883/embed.png?style=banner3)](https://discord.gg/fsofbsadw)

Инвайт код, на случай если ссылка почему-то не работает: `fsofbsadw`

***

### Приложение Discord

Скачай приложение Discord для своего ПК или смартфона. Ты сможешь использовать PTT глобально, получать push уведомления, а на ПК с Windows будет оверлей в игре, это позволит видеть кто рядом с тобой в канале, включать заглушение и менять их громкость.

Нажми для скачивания:

#### Стабильная версия

Самая стабильная версия.

[![Windows](img/windows_logo.png) Windows](https://discordapp.com/api/download?platform=win) | [![Mac OS X](img/macos_logo.png) Mac OS X](https://discordapp.com/api/download?platform=osx) | [![Android](img/android_logo.png) Android](https://play.google.com/store/apps/details?id=com.discord) | [![iOS](img/apple_logo.png) iOS](https://itunes.apple.com/us/app/discord-chat-for-games/id985746746)

#### Бета версия

Получи доступ к бета-функциям.

[![Windows](img/windows_logo.png) Windows](https://discordapp.com/api/download/ptb?platform=win) | [![Mac OS X](img/macos_logo.png) Mac OS X](https://discordapp.com/api/download/ptb?platform=osx)

[![Android](img/android_logo.png) Стать тестером бета-клиента для Android](https://play.google.com/apps/testing/com.discord)

> [![iOS](img/apple_logo.png) Тестирование бета-клиента для iOS](#nothing) (только по инвайтам)

#### Canary версия

Получи первым доступ к самым последним добавленным функциям. Тестируй и сообщайте о багах [здесь](https://discord.gg/discord-testers) (Варненг! There you should use English language!)

**Помни!** Эта версия может быть крайне не стабильной, содержать много багов и различных глюков!

[![Windows](img/windows_logo.png) Windows](https://discordapp.com/api/download/canary?platform=win) | [![Mac OS X](img/macos_logo.png) Mac OS X](https://discordapp.com/api/download/canary?platform=osx) | [![Linux](img/linux_logo.png) Linux](https://discordapp.com/api/download/canary?platform=linux)

P.S. Все ссылки ведут на официальный сайт Discord!

***

### Гайды

Кстати, я очень рекомендую прочитать тебе раздел "О сервере" [здесь](about_discord.md), там много полезной информации ![Kappa](https://static-cdn.jtvnw.net/emoticons/v1/25/1.0)