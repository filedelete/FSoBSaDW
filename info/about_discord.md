# FAN DISCORD SERVER OF BLACK SILVER AND DARIYA WILLIS

## [![Иконка Discord](img/discord_icon_24x24.png) Discord сервер](about_discord.md)

**Д**обро пожаловать сюда. Если тебя действительно заинтересовал наш сервер Discord, ты можешь вступить сюда и скорее всего ты не уйдешь, пока тебя не взбесят или не выкинут за нарушение правил. Хм.. Кстати...

***

### Правила сервера

Правила на сервере можно довольно сильно сократить:

* Вам должно быть хотя бы 16 лет и больше
* КАПС, флуд, спам, спойлеры, NSFW, политика, неаргументированная критика, оскорбления - запрещены
* Игры с ботами только в `#chaos`
* `#chaos` не для общения
* Все удаленные, измененные сообщения логируются
* Даже в войсе нужно вести себя адекватно
* Администраторы / модераторы не могут банить без причины

Расширенные правила доступны [**здесь**](../rules/discord_v3.md). **Настоятельно** рекомендуется их изучить в свободное от дел время! Помните, что незнание этих правил не освобождает вас от ответственности.

### Гайды о Discord

* [ЧаВо по Discord](discord/faq.md)
* [ЧаВо по серверу](https://docs.google.com/document/d/1EltZ6AaB5kVQcm37au5CBQgW8Savo29tqLUDK2LBD60/edit?usp=sharing)
* [Форматируем текст](discord/text_formatting.md)

### Гайды по ботам

* [Paul](discord/paul/paul_general.md) - **Официальный**
* [Rocky](discord/rocky_station/rocky_general.md) - **Официальный**
* [BlackSilverBot](discord/blacksilverbot/blacksilverbot_general.md) - **Официальный**
* [Nightbot](discord/nightbot/nightbot_general.md)
* [AIRHORN SOLUTIONS](discord/airhorn/airhorn_general.md)
* [Gravebot](discord/gravebot/gravebot_general.md)
* [Tatsumaki](discord/tatsumaki/tatsumaki_general.md)
* [Spectra](discord/spectra/spectra_general.md)
* [SteamBot](discord/streambot/streambot_general.md)
