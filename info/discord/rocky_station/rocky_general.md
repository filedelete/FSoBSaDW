# FAN DISCORD SERVER OF BLACK SILVER AND DARIYA WILLIS

## [Discord сервер](../../about_discord.md) - Боты - [Rocky! station](rocky_general.md)

**Rocky! station** - бот для проигрывания музыки в голосовых каналах. Имеет [открытый исходный код](https://github.com/Just-Some-Bots/MusicBot) и доступен для установки на своём сервере.

### Команды

#### Статус

* `!np` - текущий трек
* `!queue`- текущий трек и очередь на проигрывание

#### Очереди

* `!play [ссылка на трек]` - проиграть трек по ссылке. Ссылка может быть на YouTube, Soundcloud, Coub и другие источники поддерживаемые `youtube-dl`. Поддерживаются также прямые ссылки на аудиофайлы
* `!play [название трека]` - найти на YouTube первый попавшийся трек по искомому названию и проиграть его
* `!search <сервис> <количество> "[назвние трека]"` - ищет трек на сервисе (yt/sc/yh) в указаном количестве и спрашивает какой нужен

#### Управление

* `!volume [0-100]` - установить громкость звука (проценты)
* `!skip` - пропустить текущий трек (в случае большого количества участников открывает голосование за пропуск)
* `!pause` - приостановить проигрывание
* `!resume` - продолжить проигрывание
* `!summon` - призвать Rocky! в свой каналах
* `!disconnect` - отключить Rocky! от канала

### Лимиты

На сервере действуют лимиты на бота. Лимиты распределены на статус пользователя.

#### Обычный участник

* 30 треков максимум в очередь можно поставить
* 30 минут максимальная длительность трека
* 30 треков максимум на плейлист

#### Подписчик

* 40 треков максимум в очередь можно поставить
* 1 час максимальная длительность трека
* 50 треков максимум на плейлист

#### DJ

* 200 треков максимум в очередь можно поставить
* 3 часа - максимальная длительность трека
* 200 треков максимум на плейлист

#### Модератор

* 120 треков максимум в очередь можно поставить
* 2 часа - максимальная длительность трека
* 120 треков максимум на плейлист
* Может перемешать очередь
* Может очистить очередь
* Может добавлять в ЧС бота

#### Администрация

* **Без ограничений**
* Может перемешать очередь
* Может очистить очередь
* Может изменить никнейм бота
* Может изменить имя бота
* Может изменить аватарку бота
* Может добавлять в ЧС бота
* Есть InstaSkip (`!skip` не проводит опросы никогда)

#### Я не трогаю Rocky! (заблокированные)

* Команда `!play` заблокирована
* Команда `!skip` заблокирована
* Команда `!search` заблокирована
* 1 трек максимум
* 1 секунды - максимальная длительность трека
* Запрещены плейлисты