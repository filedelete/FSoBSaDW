# FAN DISCORD SERVER OF BLACK SILVER AND DARIYA WILLIS

## [Discord сервер](../../about_discord.md) - [Nightbot](nightbot_general.md) - [Реакции](nightbot_reactions.md)

### Реакция - удаление сообщения

Nightbot удаляет сообщения за:

* КАПС - больше 8 заглавных букв
* Флуд / Спам - много повторов одного и того же
* Ссылки на сторонние ресурсы
* Плохие фразы