# FAN DISCORD SERVER OF BLACK SILVER AND DARIYA WILLIS

## [Discord сервер](../../about_discord.md) - [Paul - Rocky!'s friend](paul_general.md)

Это музыкальный бот сделанный одним из участников сервера, а именно [Максимом DeKoniX](https://github.com/dekonix).

### Использование

Использовать бота можно только в канале #rocky_station

#### Команды для Paul - Rocky!'s friend

* `_now` - узнать, что сейчас играет
* `_play` - добавить в очередь песню с YouTube/Soundcloud
* `_stop` - очищает список песен, и останавливает бота
* `_skip` - пропускает текущую песню
* `_list` - покажет что сейчас в очереди
* `_summon` - перенесет бота к вам
* `_rand` - перемешает очередь песен