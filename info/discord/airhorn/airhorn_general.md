# FAN DISCORD SERVER OF BLACK SILVER AND DARIYA WILLIS

## [Discord сервер](../../about_discord.md) - [AIRHORN SOLUTIONS](airhorn_general.md)

На Discord сервере так же есть бот с именем `AIRHORN SOLUTIONS`, его задача погудеть и крикнуть "ДЖОН СИНА!" или помычать.

### Использование

Бота можно использовать в почти любом голосовом канале. Но команды он читает только в `#chaos`.

#### Команды

* `!airhorn` - бот входит в ваш голосовой канал и... пиликает
* `!cena` - пиликалки не хватило, давайте покричим "And his name is..", "JOHN CENA!!!", "And his name is John Cena"
* `!stan` - просто "мууууууууууууууууу"
* `!bday` - дутка на день рождения
* `!anotha` - ```Uncaught TypeError: undefined is not a function                  generator.js:369```
* `[секретная команда]` - [секретная функция]