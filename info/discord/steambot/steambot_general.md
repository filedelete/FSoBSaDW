# FAN DISCORD SERVER OF BLACK SILVER AND DARIYA WILLIS

## [Discord сервер](../../about_discord.md) - [SteamBot](steambot_general.md)

На сервере проживает много интересных ботов, Steambot один из них. Steambot позволяет легко получать информацию об играх, пользователях и предметов из Steam

### Команды для Steambot

* `steam premium` - дает вам некоторую информацию об премиуме и как его получить
* `steam help` - посылает вам список команд steambot'а
* `steam game [game]` - отправляет информацию об игре из Steam
* `steam games [game]` - список игр подходящие по [название]
* `steam user [user]` - дает вам информацию о профиле пользователя в Steam
* `steam watch help` - показывает информацию о настройке продажи игры
* `steam users [user]` - список пользователей подходящих по [название]
* `steam item [game] "item"` - дает информацию о предмете из Steam
* `steam library [user]` - показывает библиотеку пользоватeля в Steam
* `steam wishlist [user]` - показывает список желаемого
* `steam playtime [user] "game"` - показывает сколько наиграно времени в игре
* `steam random [user]` - показывает рандомный игру из библиотеки пользователя
* `steam achiev [game]` - показывает информацию о достижениях в игре из Steam
* `steam top` - показывает список самых продаваемых игр
* `steam new` - показывает список самых новых игр
* `steam specials` - получить список самых популярных скидок
* `steam upcoming` - получить список предстоящих игр
* `steam popular` - показывает список самых популярных игр
* `steam userdata` - показывают данные о количестве пользователей онлайн
* `steam removed [name]` - получает количество игр в вашей библиотеке, которые были удалены из Steam
* `steam me [user]` - установить имя пользователя по умолчанию (так что вам не придется вводить свое имя для других команд)
* `steam name [user|mention]` - проверьте имя пользователя
* `steam language [language]` - выберите ваш язык для Steam сообщений
* `steam country [countrycode]` - выбрать вашу страну для магазина Steam
* `steam currency [currency] [symbol]` - выбрать валюту для магазина Steam